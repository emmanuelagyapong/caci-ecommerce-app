from django.db import models

# Create your models here.
class email_subscription(models.Model):
    email = models.EmailField(max_length=255)

    class Meta:
        db_table = 'email_subscription'


